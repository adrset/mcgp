It is recommended to open this project in Intellij, for fast and smooth installation
without user action required.

The project consists of three main functions, each represents different usage of the project.

The first "Main" represents an example of solving Fibonnaci sequence using RCGP
The second "MainBoolean" represents an example of solving Simple 3 bit adder.
The last "MainMemory" consists of code that represents an example usage of MCGP for solving Fibonnaci sequence. (WIP)

The project consists of lib package and user package.
The lib package contains all the library classes.
The user package consists of Main functions and classes that must be implemented in order the program to understand the problem that is to be solved.

Let's take a look at the minimal requirements that must we meet:

    FunctionFactory<Double> factory = new RandomDoubleFunctionFactory();
    InputSamples<Double> dataSet = new InputSamples.Builder<Double>().setTargetClass(Double.class).setFileName("fib.json").build();
    Config config = dataSet.getConfig();
    NodeFactory<Double> nodeFactory = new NodeFactory<>(config, factory, 0.);
    this.simulation = new SimulationModel<>(config, nodeFactory, new FibEvaluator(dataSet.getSamples()), new DoubleGuard(), new RecursiveRandomConnectionMutator<>(config));
    this.simulation.init();
    this.simulation.run();

User must create a FunctionFactory that will be providing the possible operations. During mutation the factory will return a new method for a node. (Any distribution)
InputSamples is a class that loads the data from a JSON file. Please see example files in the resources.
NodeFactory is a class that will be responsible for creating new Nodes during mutation.

SimulationModel is a core class that conducts the simulation. It must be init.

    public void run() throws Exception{
        do {
            compute();
            breeding();
            mutation();
        } while (shouldRun());
    }

SimulationModel takes care of creating Individuals - our little programs we will be trying to evolute.
Each Individual consists of specified number of nodes, with a distinction on InputNode - a node that has a value predefined by certain sample; Node - a node that is connected to other nodes(inputs) and has a specific strategy for processing the given inputs; OutputNode - an output node.
The connections are implemented using ConnectionAdapter. Each node has one. The connection adapter takes care of fetching the values from connected nodes.

At the beginning the program creates 5 individuals (we use 1+4 evolution strategy).
During each generation we choose the most effective Individual. This is done using a Evaluator class. User may use any algorithm he desires.

    private void breeding() {
        List<Individual<T>> newIndividuals = new ArrayList<>();
        newIndividuals.add(theFittest);
        for (int i = 0; i < config.getIndividuals() - 1; i++) {
            Individual<T> child = theFittest.clone();
            child.setFitness(theFittest.getFitness());
            newIndividuals.add(child);
        }
        individuals = newIndividuals;
    }

    private void mutation() {
        for (int i = 0; i < config.getIndividuals(); i++) {
            Individual<T> individual = individuals.get(i);
            if (!individual.isParent()) {
                connectionMutator.mutate(individual);
                functionMutator.mutate(individual);
            }
        }
    }

    //Computing
    private void compute() throws Exception{
        service.init(individuals.size());
        List<Exception> exceptions = new ArrayList<>();
        individuals.stream().forEach(individual -> {
            individual.zero();
            
            for (Sample<T> sample : evaluator.getSamples()) {
                List<T> values = individual.compute(sample);
                if (guard != null) {
                    values = values.stream().map(e ->
                            guard.guard(e)).collect(Collectors.toList());
                }
                // Storing the computed value in Sample will be removed soon 
                sample.setComputedOutput(values);
            }
            individual.setFitness(evaluator.evaluate());
        })
        theFittest = evaluator.getFittest(individuals);
    }

Additionally, there's a Guard class that removes computer limitations like infinities (e.g. dividing by zero).
The fittest Individual is then cloned and we begin the mutation. The mutation is accomplished by two Classes that implement IMutator interface.
First Mutator takes care of mutation of Individuals' Nodes' strategy. The second changes the connections between Nodes.

