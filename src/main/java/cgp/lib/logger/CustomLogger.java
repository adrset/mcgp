package cgp.lib.logger;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.VFS;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class CustomLogger {
    File csvOutputFile;
    Map<Integer, Double> generations;
    String fileName;
    private static final String DIR = "csv/";

    public CustomLogger(String fileName) {
        try {
            Files.createDirectories(Paths.get(DIR));
        } catch (Exception e) {
        }
        this.fileName = fileName;
        csvOutputFile = new File(DIR + fileName);
        this.generations = new LinkedHashMap<>();
    }


    private String convertToCSV(int generation, double data) {
        return generation + "," + data;
    }

    public void write(int generation, Double fitness) {
        double priorValue = generations.get(generation) != null ? generations.get(generation) : 0.0;
        double priorValue2 = fitness;
        generations.put(generation, priorValue + priorValue2);
    }

    public void save() {
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            pw.println("Generation,fitness");
            for (Integer generation : generations.keySet()) {
                pw.println(convertToCSV(generation, generations.get(generation)));
            }
        } catch (FileNotFoundException exception) {
            exception.printStackTrace();
        }
    }

    public void upload() throws IOException {
        FileSystemManager manager = VFS.getManager();

        FileObject local = manager.resolveFile(
                System.getProperty("user.dir") + "/" + DIR + "/" + fileName);
        FileObject remote = manager.resolveFile(
                "" + fileName);

        remote.copyFrom(local, Selectors.SELECT_SELF);

        local.close();
        remote.close();
    }
}
