package cgp.lib.logger;

import cgp.lib.individual.Individual;
import cgp.lib.individual.pojo.samples.Sample;

import java.util.List;
import java.util.stream.Collectors;

public class OutPrinter<T> {
    public void printDetails(int currentGeneration, List<Sample<T>> samples, Individual<T> theFittest) {
        System.out.println("Min error reached! [g:" + currentGeneration + "]");
        for (Sample<T> sample : samples) {
            List<T> output = theFittest.computeSample(sample);
            String tmp;
            try {
                tmp = "[" + output.stream().map(i -> String.format("%.2f", i)).collect(Collectors.joining(",")) + "]";
                tmp += " vs [" + sample.getOutput().stream().map(i -> String.format("%.2f", i)).collect(Collectors.joining(",")) + "]";
            } catch (Exception e) {
                tmp = "[" + output.stream().map(i -> String.format("%b", i)).collect(Collectors.joining(",")) + "]";
            }
            System.out.println(tmp);
        }
        System.out.println(theFittest.getFitness());
    }


    public void printFitness(Double fitness, int generation) {
        System.out.println("\t" + fitness + "[" + generation + "]");
    }
}
