package cgp.lib.individual;

import cgp.lib.individual.pojo.samples.Sample;
import cgp.lib.node.InputNode;
import cgp.lib.node.NodeWithMemory;
import cgp.lib.node.OutputNode;
import cgp.lib.node.adapter.MemoryConnectionAdapter;
import cgp.lib.node.guard.ComputedValueGuard;
import cgp.lib.simulation.input.Config;
import cgp.lib.node.Node;
import cgp.lib.node.adapter.ConnectionAdapter;
import cgp.lib.node.factory.AbstractNodeFactory;

import java.util.*;
import java.util.stream.Collectors;

public class Individual<T> {
    private final Config config;
    private final AbstractNodeFactory<T> factory;
    protected List<Node<T>> allNodes;
    protected Node<T> outputNode;
    private boolean parent = false;
    private Set<Node<T>> activeNodes = new HashSet<>();
    private final List<Sample<T>> learningSamples;

    public List<Sample<T>> getLearningSamples() {
        return learningSamples;
    }

    public List<Sample<T>> getComputedSamples() {
        return computedSamples;
    }

    private final List<Sample<T>> computedSamples;

    private int basicNodesNo;
    private int inputNodesNo;
    private double fitness;

    private final ComputedValueGuard<T> guard;

    public boolean isParent() {
        return parent;
    }

    public void setParent(boolean parent) {
        this.parent = parent;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public double getFitness() {
        return fitness;
    }

    public Individual(Config config, AbstractNodeFactory<T> factory, List<Sample<T>> samples, ComputedValueGuard<T> guard) {
        this.guard = guard;
        this.factory = factory;
        this.config = config;
        this.learningSamples = samples;
        this.computedSamples = new ArrayList<>();
        for (Sample<T> learningSample : samples) {
            this.computedSamples.add(new Sample<>(learningSample.getInput(), new ArrayList<>()));
        }
        allNodes = new ArrayList<>();
    }

    public void zero() throws Exception {
        T defaultVal = factory.getNode().getDefaultValue();
        for (int i = inputNodesNo; i < basicNodesNo + inputNodesNo; i++) {
            Node<T> node = allNodes.get(i);
            node.setDefaultValue(defaultVal);
            if (node instanceof NodeWithMemory) {
                NodeWithMemory<T> memoryNode = (NodeWithMemory<T>) node;
                for (int j = 0; j < config.getMemoryLength(); j++) {
                    memoryNode.remember(defaultVal);
                }
            }
        }
    }

    public void init() throws Exception {
        basicNodesNo = config.getNodeAmount();
        inputNodesNo = config.getInputs();
        for (int i = 0; i < inputNodesNo; i++) {
            allNodes.add(factory.getInputNode());
        }

        for (int i = inputNodesNo; i < basicNodesNo + inputNodesNo; i++) {
            allNodes.add(factory.getNode());
        }

        // One output node that has n outputs linked as inputs
        // basically just a helper node. :)
        Node<T> outputNode = factory.getOutputNode();
        allNodes.add(outputNode);
        this.outputNode = outputNode;
    }

    public void setNodes(List<Node<T>> nodes) {
        this.allNodes = nodes;
    }

    /**
     * Computes active nodes from left to right (firstly determines which are active using respective methods).
     */
    public List<T> computeSample(Sample<T> sample) {
        resetNodesActiveStatus();
        setActiveNodes();
        setInputValues(sample);
        computeNodes();
        return getOutput();
    }

    public void computeSamples() throws Exception {
        this.zero();
        for (int i = 0; i < learningSamples.size(); i++) {
            Sample<T> sample = learningSamples.get(i);
            // do not store output in sample!!!!
            List<T> values = this.computeSample(sample);
            if (guard != null) {
                values = values.stream().map(guard::guard).collect(Collectors.toList());
            }
            computedSamples.get(i).setOutput(values);
        }
    }

    private void computeNodes() {
        //TODO: iterate to otp nodes.
        activeNodes.forEach(Node::compute);
    }

    private void setInputValues(Sample<T> s) {
        for (int i = 0; i < inputNodesNo; i++) {
            Node<T> n = allNodes.get(i);
            n.setDefaultValue(s.getInput().get(i));
        }
    }

    public List<T> getOutput() {
        List<T> values = new ArrayList<>();
        List<Node<T>> outputs = allNodes.get(allNodes.size() - 1).getAdapter().getNodes();
        for (Node<T> o : outputs) {
            values.add(o.getDefaultValue());
        }
        return values;
    }

    /**
     * Set all node statuses to inactive.
     */
    private void resetNodesActiveStatus() {
        activeNodes.clear();
        for (Node<T> allNode : allNodes) {
            allNode.setActive(false);
        }
    }

    public boolean isValid() {
        return this.activeNodes.size() > 0;
    }


    /**
     * Loops trough output nodes and recursively marks all nodes that it's connected to.
     */
    private void setActiveNodes() {
        // assuming one otp
        List<Node<T>> outputs = allNodes.get(allNodes.size() - 1).getAdapter().getNodes();
        for (Node<T> o : outputs) {
            recursivelySetActiveNodes(o);
        }
    }

    private void recursivelySetActiveNodes(Node<T> node) {
        // not checking for input nodes
        if (node instanceof InputNode) {
            return;
        }

        if (node.isActive()) {
            return;
        }

        node.setActive(true);
        activeNodes.add(node);

        ConnectionAdapter<T> adapter = node.getAdapter();
        for (Node<T> child : adapter.getNodes()) {
            recursivelySetActiveNodes(child);
        }
    }

    private void recursivelySetActiveNodes(int index) {
        // not checking for input nodes
        if (index < inputNodesNo) {
            return;
        }

        Node<T> node = allNodes.get(index);

        if (node.isActive()) {
            return;
        }

        node.setActive(true);
        activeNodes.add(node);

        ConnectionAdapter<T> adapter = node.getAdapter();
        for (Node<T> child : adapter.getNodes()) {
            int childIndex = allNodes.indexOf(child);
            recursivelySetActiveNodes(childIndex);
        }
    }

    public void traverseNodes() {
        Deque<Node<T>> stack = new ArrayDeque<>();
        Set<Node<T>> visited = new HashSet<>();
        stack.push(outputNode);
        boolean first = true;
        while (!stack.isEmpty()) {
            Node<T> node = stack.pop();
            if (!first) {
                visited.add(node);
            } else {
                first = false;
            }
            ConnectionAdapter<T> adapter = node.getAdapter();

            List<Node<T>> adjacentNodes = adapter.getNodes();
            for (Node<T> adjacentNode : adjacentNodes) {
                if (!visited.contains(adjacentNode) && !(adjacentNode instanceof InputNode) && !(adjacentNode instanceof OutputNode)) {
                    stack.push(adjacentNode);
                    adjacentNode.setActive(true);
                }
            }

        }
        activeNodes = visited;
    }

    @Override
    public Individual<T> clone() {
        Individual<T> clone = new Individual<>(this.config, this.factory, this.learningSamples, this.guard);
        Map<Integer, Node<T>> nodeMap = new HashMap<>();
        for (Node<T> theNode : allNodes) {
            Node<T> clon = theNode.clone();
            nodeMap.put(clon.getUID(), clon);
            clone.allNodes.add(clon);
        }
        clone.outputNode = clone.allNodes.get(clone.allNodes.size() - 1);

        // Now let's create new adapters and recreate them basing on original ones
        for (int i = 0; i < this.allNodes.size(); i++) {
            Node<T> currentNode = this.allNodes.get(i);
            List<Node<T>> inputs = currentNode.getAdapter().getNodes();
            ConnectionAdapter<T> oldAdapter = currentNode.getAdapter();
            ConnectionAdapter<T> adapter = null;
            if (oldAdapter instanceof MemoryConnectionAdapter) {
                adapter = new MemoryConnectionAdapter<>(currentNode.getAdapter().getMaxArity());
                ((MemoryConnectionAdapter<T>) adapter).setInputLocations(new ArrayList<>(((MemoryConnectionAdapter<T>) oldAdapter).getInputLocations()));
            } else {
                adapter = new ConnectionAdapter<>(currentNode.getAdapter().getMaxArity());

            }
            List<Node<T>> newInputs = new ArrayList<>();

            for (Node<T> id : inputs) {
                if (id == null) {
                    //Basically means that no node is connected there
                    continue;
                }
                Node<T> foundNode = nodeMap.get(id.getUID());
                // Node<T> foundNode = getNodeWithUID(nodesCopy, id.getUID());
                newInputs.add(foundNode);
            }
            adapter.setInputs(newInputs);
            clone.allNodes.get(i).setAdapter(adapter);

        }
        clone.parent = false;

        clone.inputNodesNo = this.inputNodesNo;
        clone.basicNodesNo = this.basicNodesNo;

        return clone;
    }

    public String toString() {
        return activeNodes != null ? activeNodes.stream()
                .map(object -> Objects.toString(object, null))
                .collect(Collectors.joining(" -> ")) : "no active nodes";
    }

    public List<Node<T>> getAllNodes() {
        return allNodes;
    }
}
