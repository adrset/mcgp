package cgp.lib.function.factory;

import cgp.lib.function.method.ArityFunction;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public abstract class FunctionFactory<T> {
    protected Map<Class<?>, ElementFactory> elementBuilder = new HashMap<>();

    public abstract ArityFunction<T> getFunction() throws Exception;
}
