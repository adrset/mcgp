package cgp.lib.simulation.mutator.connection.memory;

import cgp.lib.individual.Individual;
import cgp.lib.node.Node;
import cgp.lib.node.NodeWithMemory;
import cgp.lib.node.adapter.ConnectionAdapter;
import cgp.lib.node.adapter.MemoryConnectionAdapter;
import cgp.lib.simulation.input.Config;
import cgp.lib.simulation.mutator.IMutator;

import java.util.List;
import java.util.Random;

public class RandomMemoryConnectionMutator<T> implements IMutator<T> {
    protected static Random generator = new Random();
    protected Config config;

    public RandomMemoryConnectionMutator(Config config) {
        this.config = config;
    }

    @Override
    public void mutate(Individual<T> individual) {
        List<Node<T>> nodes = individual.getAllNodes();

        for (int ii = config.getInputs(); ii < nodes.size() - config.getOutputs(); ii++) {
            ConnectionAdapter<T> adapter = nodes.get(ii).getAdapter();

            List<Node<T>> adapterNodes = adapter.getNodes();
            for (int kk = 0; kk < adapterNodes.size(); kk++) {
                // Mutate?
                if (shouldMutate(config.getMutationProbability())) {
                    // Memory connection?
                    if (shouldMutate(config.getMemoryConnectionProbability())) {
                        memoryMutation(kk, (MemoryConnectionAdapter<T>) adapter, nodes.subList(0, ii));
                    } else if (shouldMutate(config.getRecursiveConnectionProbability())) {
                        updateNode(kk, adapter, nodes.subList(ii, nodes.size() - config.getOutputs()));
                    } else {
                        updateNode(kk, adapter, nodes.subList(0, ii));
                    }
                }
            }
        }
    }

    private static Boolean shouldMutate(float probability) {
        double randomDouble = generator.nextDouble();
        return randomDouble < probability;
    }

    private void memoryMutation(int kk, MemoryConnectionAdapter<T> adapter, List<Node<T>> availableNodes) {
        List<Node<T>> adapterNodes = adapter.getNodes();
        updateNode(kk, adapter, availableNodes);
        Node<T> theNode = adapterNodes.get(kk);
        List<Integer> inputLocations = adapter.getInputLocations();
        int location = theNode instanceof NodeWithMemory ? generator.nextInt(config.getMemoryLength()) : 0;
        inputLocations.set(kk, location);
    }

    protected void updateNode(int index, ConnectionAdapter<T> adapter, List<Node<T>> availableNodes) {
        List<Node<T>> adapterNodes = adapter.getNodes();
        Node<T> newConnectedNode = getRandomNode(availableNodes);
        adapterNodes.set(index, newConnectedNode);
        MemoryConnectionAdapter<T> memoryAdapter = (MemoryConnectionAdapter<T>) adapter;
        List<Integer> inputLocations = memoryAdapter.getInputLocations();
        //was inputLocations.set(index, config.getMemoryLength() - 1);
        // TODO: review
        inputLocations.set(index, newConnectedNode instanceof NodeWithMemory ? config.getMemoryLength() - 1 : 0);
    }

    protected Node<T> getRandomNode(List<Node<T>> nodes) {
        int randomIndex1 = generator.nextInt(nodes.size());
        return nodes.get(randomIndex1);
    }
}
