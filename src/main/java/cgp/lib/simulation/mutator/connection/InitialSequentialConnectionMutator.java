package cgp.lib.simulation.mutator.connection;

import cgp.lib.individual.Individual;
import cgp.lib.node.Node;
import cgp.lib.node.adapter.ConnectionAdapter;
import cgp.lib.simulation.input.Config;

import java.util.List;

public class InitialSequentialConnectionMutator<T> extends RandomConnectionMutator<T> {
    public InitialSequentialConnectionMutator(Config params) {
        super(params);
    }

    @Override
    public void mutate(Individual<T> individual) {
        List<Node<T>> nodes = individual.getAllNodes();

        for (int ii = config.getInputs(); ii < nodes.size(); ii++) {
            Node<T> node = nodes.get(ii);

            ConnectionAdapter<T> adapter = node.getAdapter();
            List<Node<T>> adapterNodes = adapter.getNodes();
            List<Node<T>> availableNodes = nodes.subList(ii-1, ii);
            for (int kk = 0; kk < adapterNodes.size(); kk++) {
                super.updateNode(kk, adapterNodes, availableNodes.get(0));
            }
        }
    }


}
