package cgp.lib.simulation.mutator;

import cgp.lib.individual.Individual;
import cgp.lib.node.Node;

import java.util.List;

public interface IMutator <T> {
    void mutate (Individual<T> individual);
}
