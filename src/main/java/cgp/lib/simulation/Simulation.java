package cgp.lib.simulation;

import cgp.lib.function.factory.FunctionFactory;
import cgp.lib.individual.Individual;
import cgp.lib.logger.CustomLogger;
import cgp.lib.logger.OutPrinter;
import cgp.lib.node.factory.MemoryNodeFactory;
import cgp.lib.node.factory.NodeFactory;
import cgp.lib.node.guard.ComputedValueGuard;
import cgp.lib.simulation.evaluation.AbstractEvaluate;
import cgp.lib.simulation.input.Config;
import cgp.lib.simulation.mutator.IMutator;
import cgp.lib.simulation.mutator.connection.InitialRandomConnectionMutator;
import cgp.lib.simulation.mutator.connection.InitialSequentialConnectionMutator;
import cgp.lib.simulation.mutator.connection.RandomConnectionMutator;
import cgp.lib.simulation.mutator.connection.memory.RandomMemoryConnectionMutator;
import cgp.lib.simulation.mutator.connection.resursive.RecursiveRandomConnectionMutator;
import cgp.lib.simulation.mutator.function.FunctionMutator;

import java.util.*;
import java.util.stream.Collectors;

public class Simulation<T> {
    OutPrinter<T> printer;
    int iterations;
    protected Random generator;
    protected List<Individual<T>> individuals;

    protected Config config;
    protected NodeFactory<T> nodeFactory;

    protected IMutator<T> connectionMutator;
    protected IMutator<T> initialConnectionSetter;
    protected FunctionMutator<T> functionMutator;
    protected AbstractEvaluate<T> evaluator;
    protected Individual<T> theFittest;
    protected ComputedValueGuard<T> guard;
    protected int currentGeneration;
    protected int currentIteration;
    protected CustomLogger csvLogger;
    protected CustomLogger csvAggregateLogger;
    protected final static int LOG_FREQUENCY = 1000;
    protected int csvLogFrequency;
    private String fileName;
    List<Exception> exceptions = new ArrayList<>();
    public enum Type {CGP, RCGP, MCGP}
    List<Individual<T>> newIndividuals = new ArrayList<>();


    public Simulation(Builder<T> builder) {
        generator = new Random();
        config = builder.config;
        fileName = builder.fileName;
        csvLogFrequency = builder.csvLogFrequency;
        csvAggregateLogger = new CustomLogger(fileName);
        initialConnectionSetter = builder.randomInitialConnection? new InitialRandomConnectionMutator<>(config) : new InitialSequentialConnectionMutator<>(config);
        functionMutator = new FunctionMutator<>(config, builder.factory);
        evaluator = builder.evaluator;
        guard = builder.guard;
        iterations = builder.iterations;
        printer = new OutPrinter<>();
        switch (builder.type) {
            case RCGP:
                connectionMutator = new RecursiveRandomConnectionMutator<>(config);
                nodeFactory = new NodeFactory<>(config, builder.factory, builder.defaultValue);
                break;
            case MCGP:
                connectionMutator = new RandomMemoryConnectionMutator<>(config);
                nodeFactory = new MemoryNodeFactory<T>(config, builder.factory, builder.defaultValue);
                break;
            case CGP:
                connectionMutator = new RandomConnectionMutator<>(config);
                nodeFactory = new NodeFactory<>(config, builder.factory, builder.defaultValue);
                break;
        }
    }

    public void start() throws Exception {
        for (int i = 0; i < iterations; i++) {
            this.init();
            this.run();
            csvLogger.save();
            csvLogger.upload();
        }
        csvAggregateLogger.save();
        csvAggregateLogger.upload();
    }

    private void run() throws Exception {
        do {
            compute();
            breeding();
            mutation();
        } while (shouldRun());
    }

    private boolean shouldRun() throws Exception {
        boolean shouldRun = true;
        currentGeneration++;
        if (currentGeneration % LOG_FREQUENCY == 0) {
            printer.printFitness(theFittest.getFitness(), currentGeneration);
            csvLogger.write(currentGeneration, theFittest.getFitness());
            csvAggregateLogger.write(currentGeneration, theFittest.getFitness());
        }

        if (theFittest.getFitness() < config.getMinError()) {
            // TODO: Should not zero it!
            theFittest.zero();
            printer.printDetails(currentGeneration, evaluator.getSamples(), theFittest);
            shouldRun = false;
        }

        if (currentGeneration > config.getGenerationThreshold()) {
            shouldRun = false;
        }

        return shouldRun;
    }

    private void breeding() {
        individuals.clear();
        individuals.add(theFittest);
        for (int i = 0; i < config.getIndividuals() - 1; i++) {
            Individual<T> child = theFittest.clone();
            child.setFitness(theFittest.getFitness());
            individuals.add(child);
        }
    }

    private void mutation() {
        for (int i = 0; i < config.getIndividuals(); i++) {
            Individual<T> individual = individuals.get(i);
            if (!individual.isParent()) {
                connectionMutator.mutate(individual);
                functionMutator.mutate(individual);
            }
        }
    }

    private void init() throws Exception {
        this.individuals = new ArrayList<>();
        this.currentGeneration = 0;
        this.currentIteration += 1;
        this.csvLogger = new CustomLogger("iter_" + this.currentIteration + "_" + this.fileName);
        for (int ii = 0; ii < config.getIndividuals(); ii++) {
            Individual<T> individual = new Individual<>(config, nodeFactory, evaluator.getSamples(), guard);
            individual.init();
            initialConnectionSetter.mutate(individual);
            individuals.add(individual);
        }
    }

    protected void compute() throws Exception {
        individuals.parallelStream().forEach(individual -> {
            try {
                individual.computeSamples();
                individual.setFitness(evaluator.evaluate(individual));
            } catch (Exception e) {
                exceptions.add(e);
            }
        });

        if (exceptions.size() > 0) {
            throw exceptions.get(0);
        }
        theFittest = evaluator.getFittest(individuals);
    }

    public Individual<T> getFittest() {
        return theFittest;
    }


    public static final class Builder<T> {
        Type type;
        AbstractEvaluate<T> evaluator;
        ComputedValueGuard<T> guard;
        FunctionFactory<T> factory;
        Config config;
        T defaultValue;
        String fileName;
        int csvLogFrequency = 1000;
        Boolean randomInitialConnection = true;
        int iterations = 1;

        static public <T> Builder<T> start() {
            return new Builder<T>();
        }

        public Builder<T> type(Type type) {
            this.type = type;
            return this;
        }

        public Builder<T> iterations(int iterations) {
            this.iterations = iterations;
            return this;
        }

        public Builder<T> fileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public Builder<T> randomInitialConnection(Boolean randomInitialConnection) {
            this.randomInitialConnection = randomInitialConnection;
            return this;
        }

        public Builder<T> config(Config config) {
            this.config = config;
            return this;
        }

        public Builder<T> defaultValue(T defaultValue) {
            this.defaultValue = defaultValue;
            return this;
        }

        public Builder<T> factory(FunctionFactory<T> factory) {
            this.factory = factory;
            return this;
        }

        public Builder<T> guard(ComputedValueGuard<T> guard) {
            this.guard = guard;
            return this;
        }

        public Builder<T> csvLogFrequency(int csvLogFrequency) {
            this.csvLogFrequency = csvLogFrequency;
            return this;
        }

        public Builder<T> evaluator(AbstractEvaluate<T> evaluator) {
            this.evaluator = evaluator;
            return this;
        }

        public Simulation<T> build() {
            if (Arrays.stream(new Object[]{evaluator, config, factory, defaultValue, fileName}).anyMatch(Objects::isNull)) {
                throw new IllegalStateException("Name cannot be empty");
            }

            return new Simulation<T>(this);
        }
    }
}
