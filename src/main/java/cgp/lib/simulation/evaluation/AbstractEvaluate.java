package cgp.lib.simulation.evaluation;

import cgp.lib.individual.Individual;
import cgp.lib.individual.pojo.samples.Sample;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractEvaluate<T> {
    public abstract double evaluate(Individual<T> individual);

    private static final Double MIN_ERROR = 0.000001d;

    protected List<Sample<T>> samples;

    public Individual<T> getFittest(List<Individual<T>> individuals) {
        Individual<T> fittest = getParent(individuals);
        for (Individual<T> next : individuals) {
            if (Double.isNaN(next.getFitness()) || Double.isInfinite(next.getFitness())) {
                continue;
            }
            if (!next.isValid()) {
                continue;
            }
            if (next.getFitness() < fittest.getFitness()) {
                fittest = next;
            } else if (!next.isParent() && fittest.isParent() && Math.abs(next.getFitness() - fittest.getFitness()) < MIN_ERROR) {
                fittest = next;
            }
        }

        individuals.forEach(ind ->
                ind.setParent(false)
        );
        fittest.setParent(true);

        return fittest;
    }

    private Individual<T> getParent(List<Individual<T>> individuals) {
        Individual<T> fittest = null;
        List<Individual<T>> fittests = individuals.stream().filter(Individual::isParent).collect(Collectors.toList());
        if (fittests.size() > 0) {
            fittest = fittests.get(0);
        } else {
            fittest = individuals.get(0);
        }
        return fittest;
    }


    public AbstractEvaluate(List<Sample<T>> samples) {
        this.samples = samples;
    }

    public List<Sample<T>> getSamples() {
        return samples;
    }
}
