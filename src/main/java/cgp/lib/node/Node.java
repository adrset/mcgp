package cgp.lib.node;

import cgp.lib.function.method.ArityFunction;
import cgp.lib.node.adapter.ConnectionAdapter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Node<T> {
    ArityFunction<T> strategy;
    ConnectionAdapter<T> adapter;
    private static int counter = 0;

    private boolean active = false;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public T getDefaultValue() {
        return defaultValue;
    }


    public void setDefaultValue(T defaultValue) {
        this.defaultValue = defaultValue;
    }

    protected int UID;

    T defaultValue;

    public Node(ArityFunction<T> fun, ConnectionAdapter<T> adapter, T defaultValue) {
        this.strategy = fun;
        this.adapter = adapter;
        this.defaultValue = defaultValue;
    }

    /**
     * Use only for completely new nodes!!! Not cloned ones!!!
     */
    public void init() {
        this.UID = counter++;
    }

    /**
     * Only for copy!!!
     */
    public Node() {
    }

    public void setStrategy(ArityFunction<T> f) {
        this.strategy = f;
    }

    public ArityFunction<T> getStrategy() {
        return strategy;
    }

    public ConnectionAdapter<T> getAdapter() {
        return adapter;
    }

    public void setAdapter(ConnectionAdapter<T> adapter) {
        this.adapter = adapter;
    }

    public int getUID() {
        return UID;
    }

    public void setUID(int uid) {
        this.UID = uid;
    }

    public T compute(){
        List<T> inputs = adapter.getInputValues();
        defaultValue = strategy.calculate(inputs);
        return defaultValue;
    }

    public T getValue() {
        return defaultValue;
    }

    @Override
    public Node<T> clone() {
        Node<T> clone = new Node<>();
        if (this.strategy != null) {
            //TODO: try to use existing factory here?
            clone.setStrategy((ArityFunction<T>) this.strategy.clone());
        }
        clone.setUID(this.UID);
        clone.defaultValue = this.defaultValue;

        return clone;
    }

    @Override
    public String toString(){
        return strategy.toString() + "(" + adapter.toString() +  ")";
    }
}
