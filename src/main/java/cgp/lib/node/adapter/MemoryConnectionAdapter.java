package cgp.lib.node.adapter;

import cgp.lib.node.Node;
import cgp.lib.node.NodeWithMemory;

import java.util.ArrayList;
import java.util.List;

public class MemoryConnectionAdapter<T> extends ConnectionAdapter<T> {
    List<Integer> inputLocations;
    List<T> inputValues = new ArrayList<>();
    public MemoryConnectionAdapter(int maxArity) {
        super(maxArity);
        // Setting default location
        inputLocations = new ArrayList<>();

        for (int ii = 0; ii < maxArity; ii++) {
            inputLocations.add(0);
        }
    }

    public List<Integer> getInputLocations() {
        return inputLocations;
    }

    @Override
    public List<T> getInputValues(){
        inputValues = new ArrayList<>();
        for (int i = 0; i < inputs.size(); i++) {
            Node<T> inputNode = inputs.get(i);
            if (inputNode instanceof NodeWithMemory) {
                NodeWithMemory<T> memoryNode = (NodeWithMemory<T>) inputNode;
                inputValues.add(memoryNode.getMemorisedValue(inputLocations.get(i)));
            } else {
                inputValues.add(inputNode.getDefaultValue());
            }
        }

        return inputValues;
    }

    public void setInputLocations(List<Integer> inputLocations) {
        this.inputLocations = inputLocations;
    }

    @Override
    public MemoryConnectionAdapter<T> clone() {
        MemoryConnectionAdapter<T> ca = new MemoryConnectionAdapter<>(this.inputs.size());
        List<Node<T>> clone = new ArrayList<>(this.inputs);
        for (Node<T> node : super.inputs) {
            clone.add(node.clone());
        }
        ca.setInputs(clone);
        ca.inputLocations = new ArrayList<>(inputLocations);
        return ca;
    }
}
