package cgp.lib.node.factory;

import cgp.lib.function.factory.FunctionFactory;
import cgp.lib.simulation.input.Config;
import cgp.lib.simulation.mutator.IMutator;
import cgp.lib.node.Node;

public abstract class AbstractNodeFactory<T> {
    FunctionFactory<T> factory;
    Config config;

    public AbstractNodeFactory(Config config, FunctionFactory<T> factory) {
        this.factory = factory;
        this.config = config;
    }

    public FunctionFactory<T> getFunctionFactory() {
        return factory;
    }

    public abstract Node<T> getNode() throws Exception;

    public abstract Node<T> getInputNode() throws Exception;

    public abstract Node<T> getOutputNode() throws Exception;
}
