package cgp.lib.node.factory;

import cgp.lib.function.factory.FunctionFactory;
import cgp.lib.node.InputNode;
import cgp.lib.node.OutputNode;
import cgp.lib.node.Node;
import cgp.lib.node.adapter.ConnectionAdapter;
import cgp.lib.simulation.input.Config;

public class NodeFactory<T> extends AbstractNodeFactory<T> {
    T defaultValue;
    public NodeFactory(Config config, FunctionFactory<T> factory, T defaultValue) {
        super(config, factory);
        this.defaultValue = defaultValue;
    }

    public Node<T> getNode() throws Exception {
        Node<T> n = new Node<>(factory.getFunction(), new ConnectionAdapter<>(config.getMaxArity()), defaultValue);
        n.init();
        return n;
    }

    @Override
    public Node<T> getInputNode() throws Exception {
        Node<T> n = new InputNode<>(defaultValue);
        n.init();
        return n;
    }

    @Override
    public Node<T> getOutputNode() throws Exception {
        OutputNode<T> n = new OutputNode<>(factory.getFunction(), new ConnectionAdapter(config.getOutputs()), defaultValue);
        n.init();
        return n;
    }

}
