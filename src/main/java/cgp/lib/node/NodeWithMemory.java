package cgp.lib.node;

import cgp.lib.function.method.ArityFunction;
import cgp.lib.node.adapter.MemoryConnectionAdapter;

import java.util.*;

public class NodeWithMemory<T> extends Node<T> {
    private LinkedList<T> memorisedValues;
    private int memoryLength;

    public NodeWithMemory(ArityFunction<T> fun, MemoryConnectionAdapter<T> adapter, T defaultValue, int memoryLength) {
        super(fun, adapter, defaultValue);
        this.memorisedValues = new LinkedList<>();
        this.memoryLength = memoryLength;
    }

    public NodeWithMemory() {
        this.memorisedValues = new LinkedList<>();
    }

    /**
     * Sets the default value and populates the memorisedValues with it
     * @param defaultValue
     */
    @Override
    public void setDefaultValue(T defaultValue) {
        // Populate memory register with default values
        this.defaultValue = defaultValue;
        for (int i = 0; i < memoryLength; i++) {
            remember(super.defaultValue);
        }
    }

    @Override
    public T compute() {
        T value = super.compute();
        remember(value);
        return value;
    }

    public T getMemorisedValue(int index) {
        return this.memorisedValues.get(index);
    }

    public void remember(T val) {
        if (memorisedValues.size() == memoryLength) {
            memorisedValues.removeFirst();
        }
        memorisedValues.add(val);
    }

    @Override
    public T getValue() {
        return memorisedValues.get(memoryLength - 1);
    }

    @Override
    public Node<T> clone() {
        NodeWithMemory<T> clone = new NodeWithMemory<>();
        if (this.strategy != null) {
            clone.setStrategy((ArityFunction<T>) this.strategy.clone());
        }
        clone.setUID(this.UID);
        clone.defaultValue = this.defaultValue;
        clone.memoryLength = this.memoryLength;
        for (int i = 0; i < memoryLength; i++) {
            clone.remember(this.memorisedValues.get(i));
        }
        return clone;
    }
}
