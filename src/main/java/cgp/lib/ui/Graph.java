package cgp.lib.ui;

import cgp.lib.individual.Individual;
import cgp.lib.node.InputNode;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class Graph<T> {

    private static final String fileName = "ui.css";
    private final SingleGraph graph;
    List<Edge> edges = new ArrayList<>();


    public Graph() throws IOException {
        System.setProperty("org.graphstream.ui", "swing");

        graph = new SingleGraph("CGP");
        graph.setAttribute("ui.quality");
        graph.setAttribute("ui.antialias");
        graph.setStrict(false);
        graph.setAutoCreate(false);
        graph.setAttribute("ui.stylesheet", getStylesheet());
        Viewer viewer = graph.display();
        viewer.disableAutoLayout();
    }

    public void update(Individual<T> theFittest, int currentGeneration) {
        graph.setAttribute("ui.title", "Generation " + currentGeneration);
        graph.setAttribute("ui.label", "Generation " + currentGeneration);
        List<cgp.lib.node.Node<T>> nodes = theFittest.getAllNodes();
        cgp.lib.node.Node<T> lastActiveNode = nodes.get(0);
        for (int i = 0; i < nodes.size(); i++) {
            cgp.lib.node.Node<T> node = nodes.get(i);
            Node n = graph.addNode(String.valueOf(node.getUID()));
            n.setAttribute("x", 10 * i);
            n.setAttribute("y", i % 5 * 20 - 10);
            lastActiveNode = setUiClasses(i, node, n, lastActiveNode);
        }

        addFitnessNode(theFittest, nodes);

        graph.addNode(String.valueOf(lastActiveNode.getUID())).setAttribute("ui.class", "output");
        for (int i = 0; i < edges.size(); i++) {
            try {
                graph.removeEdge(edges.get(i));
            } catch (Exception e) {

            }
        }

        for (int i = 0; i < nodes.size(); i++) {
            cgp.lib.node.Node<T> node = nodes.get(i);
            if (node.isActive()) {
                for (cgp.lib.node.Node<T> adapterNode : node.getAdapter().getNodes()) {
                    if (adapterNode != null) {
                        if (adapterNode.isActive()) {
                            edges.add(graph.addEdge(getEdgeName(node, adapterNode), String.valueOf(adapterNode.getUID()), String.valueOf(node.getUID())));
                        } else {
                            Edge e = graph.addEdge(getEdgeName(node, adapterNode), String.valueOf(adapterNode.getUID()), String.valueOf(node.getUID()));
                            e.setAttribute("ui.class", "inactive");
                            edges.add(e);
                        }
                    }
                }
            }
        }

    }

    private void addFitnessNode(Individual<T> theFittest, List<cgp.lib.node.Node<T>> nodes) {
        Node fitnessNode = graph.addNode("FITNESS NODE");
        fitnessNode.setAttribute("x", 10 * nodes.size() / 2);
        fitnessNode.setAttribute("y", -50);
        fitnessNode.setAttribute("ui.label", "fitness: " + theFittest.getFitness());
        fitnessNode.setAttribute("ui.class", "white");
    }

    private String getEdgeName(cgp.lib.node.Node<T> node1, cgp.lib.node.Node<T> node2) {
        return node1.getUID() + "<->" + node2.getUID();
    }


    private cgp.lib.node.Node<T> setUiClasses(int i, cgp.lib.node.Node<T> node, Node n, cgp.lib.node.Node<T> lastActiveNode) {
        n.setAttribute("ui.class", "");
        if (node instanceof InputNode) {
            n.setAttribute("ui.class", "input");
            n.setAttribute("ui.label", "input " + i);
        } else {
            n.setAttribute("ui.label", node.getStrategy().toString());
        }
        if (node.isActive()) {
            return node;
        }
        if (!node.isActive() && !(node instanceof InputNode)) {
            n.setAttribute("ui.class", "inactive");
        }

        return lastActiveNode;
    }

    static String getStylesheet() throws IOException {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        try (InputStream is = classLoader.getResourceAsStream(fileName)) {
            if (is == null) return null;
            try (InputStreamReader isr = new InputStreamReader(is);
                 BufferedReader reader = new BufferedReader(isr)) {
                return reader.lines().collect(Collectors.joining(System.lineSeparator()));
            }
        }
    }
}
