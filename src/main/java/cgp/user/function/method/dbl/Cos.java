package cgp.user.function.method.dbl;

import cgp.lib.function.method.ArityFunction;

import java.util.List;

public class Cos implements ArityFunction<Double> {

    @Override
    public Double calculate(List<Double> args) {
        return Math.cos(args.get(0));
    }

    @Override
    public Cos clone() {
        return new Cos();
    }

    @Override
    public String toString() {
        return "Cos";
    }
}
