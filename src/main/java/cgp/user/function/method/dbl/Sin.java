package cgp.user.function.method.dbl;

import cgp.lib.function.method.ArityFunction;

import java.util.List;

public class Sin implements ArityFunction<Double> {

    @Override
    public Double calculate(List<Double> args) {
        return Math.sin(args.get(0));
    }

    @Override
    public Sin clone() {
        return new Sin();
    }

    @Override
    public String toString() {
        return "Sin";
    }
}
