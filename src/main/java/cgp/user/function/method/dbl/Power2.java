package cgp.user.function.method.dbl;

import cgp.lib.function.method.ArityFunction;

import java.util.List;

public class Power2 implements ArityFunction<Double> {

    @Override
    public Double calculate(List<Double> args) {
        return args.get(0) * args.get(0);
    }

    @Override
    public Power2 clone() {
        return new Power2();
    }

    @Override
    public String toString() {
        return "Power2";
    }
}
