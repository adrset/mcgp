package cgp.user.function.method.dbl;

import cgp.lib.function.method.ArityFunction;

import java.util.List;

public class Exp implements ArityFunction<Double> {

    @Override
    public Double calculate(List<Double> args) {
        return Math.exp(args.get(0));
    }

    @Override
    public Exp clone() {
        return new Exp();
    }

    @Override
    public String toString() {
        return "Exp";
    }
}
