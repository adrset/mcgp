package cgp.user.function.method.dbl;

import cgp.lib.function.method.ArityFunction;

import java.util.List;

public class Power3 implements ArityFunction<Double> {

    @Override
    public Double calculate(List<Double> args) {
        return args.get(0) * args.get(0) * args.get(0);
    }

    @Override
    public Power3 clone() {
        return new Power3();
    }

    @Override
    public String toString() {
        return "Power3";
    }
}
