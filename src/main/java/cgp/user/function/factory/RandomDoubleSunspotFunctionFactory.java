package cgp.user.function.factory;

import cgp.lib.function.factory.RandomFunctionFactory;
import cgp.user.function.method.dbl.*;

public class RandomDoubleSunspotFunctionFactory extends RandomFunctionFactory<Double> {
    {
        // Use reflections in order to automatically scan for classes that are ArityFunctions
        elementBuilder.put(Add.class, Add::new);
        elementBuilder.put(Divide.class, Divide::new);
        elementBuilder.put(Multiply.class, Multiply::new);
        elementBuilder.put(Subtract.class, Subtract::new);
        elementBuilder.put(Cos.class, Cos::new);
        elementBuilder.put(Sin.class, Sin::new);
        elementBuilder.put(Exp.class, Exp::new);
        elementBuilder.put(Power2.class, Power2::new);
        elementBuilder.put(Power3.class, Power3::new);
    }
}
