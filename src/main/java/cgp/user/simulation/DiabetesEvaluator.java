package cgp.user.simulation;

import cgp.lib.individual.Individual;
import cgp.lib.individual.pojo.samples.Sample;
import cgp.lib.simulation.evaluation.AbstractEvaluate;

import java.util.List;

public class DiabetesEvaluator extends AbstractEvaluate<Double> {

    private static final Double MIN_ERROR = 0.000001d;

    public double evaluate(Individual<Double> individual) {
        int TP = 0, TN= 0, FP = 0, FN = 0;
        for (int i = 0; i < individual.getLearningSamples().size(); i++) {
            Sample<Double> learningSample = individual.getLearningSamples().get(i);
            Sample<Double> computedSample = individual.getComputedSamples().get(i);
            for (int j = 0; j < learningSample.getOutput().size(); j++) {
                Double computed = computedSample.getOutput().get(j);
                Double answer = learningSample.getOutput().get(j);
                boolean matching = Math.abs(computed - answer) < MIN_ERROR;
                if (matching) {
                    if (Math.abs(answer - 0) < MIN_ERROR) {
                        TN += 1;
                    } else {
                        TP += 1;
                    }
                } else {
                    if (Math.abs(computed - 0) < MIN_ERROR) {
                        FN += 1;
                    } else {
                        FP += 1;
                    }
                }
            }
        }

        double mcc = (TP*TN - FP* FN);
        long test1 = (long) (TP + FP) *(TP+FN);
        long test2 = (long) (TN + FP) *(TN+FN);
        double sqrt = Math.sqrt(test1 * test2);
        double fitness;
        try {
            fitness =(1 - Math.abs(mcc/sqrt));
        } catch (Exception e) {
            fitness = 10000;
        }
        return fitness;
    }

    public DiabetesEvaluator(List<Sample<Double>> samples) {
        super(samples);
    }
}
