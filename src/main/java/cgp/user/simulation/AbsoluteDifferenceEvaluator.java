package cgp.user.simulation;

import cgp.lib.individual.Individual;
import cgp.lib.individual.pojo.samples.Sample;
import cgp.lib.simulation.evaluation.AbstractEvaluate;

import java.util.List;

public class AbsoluteDifferenceEvaluator extends AbstractEvaluate<Double> {
    public double evaluate(Individual<Double> individual) {
        double fitness = 0;
        int samplesSize = individual.getLearningSamples().size();
        for (int i = 0; i < samplesSize; i++) {
            Sample<Double> learningSample = individual.getLearningSamples().get(i);
            Sample<Double> computedSample = individual.getComputedSamples().get(i);
            // one output
            fitness += Math.abs(computedSample.getOutput().get(0) - learningSample.getOutput().get(0));
        }

        return fitness / samplesSize;
    }

    public AbsoluteDifferenceEvaluator(List<Sample<Double>> samples) {
        super(samples);
    }
}
