package cgp.user.simulation;

import cgp.lib.individual.Individual;
import cgp.lib.individual.pojo.samples.Sample;
import cgp.lib.simulation.evaluation.AbstractEvaluate;

import java.util.List;

public class FibEvaluator extends AbstractEvaluate<Double> {
    private static final Integer BIG_PRIZE = 0;
    private static final Integer SMALL_PRIZE = 10;
    private static final Integer BIG_PENALTY = 1000;
    private static final Integer SMALL_PENALTY = 100;
    private static final Double MIN_ERROR = 0.000001d;

    public double evaluate(Individual<Double> individual) {
        double fitness = 0;
        boolean errorOccurred = false;
        for (int i = 0; i < individual.getLearningSamples().size(); i++) {
            Sample<Double> learningSample = individual.getLearningSamples().get(i);
            Sample<Double> computedSample = individual.getComputedSamples().get(i);
            for (int j = 0; j < learningSample.getOutput().size(); j++) {
                boolean matching = Math.abs(computedSample.getOutput().get(j) - learningSample.getOutput().get(j)) < MIN_ERROR;
                if (matching) {
                    fitness += errorOccurred ? SMALL_PRIZE : BIG_PRIZE;
                } else {
                    fitness += errorOccurred ? SMALL_PENALTY : BIG_PENALTY;
                    errorOccurred = true;
                }
            }
        }

        return fitness;
    }

    public FibEvaluator(List<Sample<Double>> samples) {
        super(samples);
    }
}
