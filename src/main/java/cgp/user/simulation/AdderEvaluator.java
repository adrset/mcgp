package cgp.user.simulation;

import cgp.lib.individual.Individual;
import cgp.lib.individual.pojo.samples.Sample;
import cgp.lib.simulation.evaluation.AbstractEvaluate;

import java.util.List;

public class AdderEvaluator extends AbstractEvaluate<Boolean> {
    public double evaluate(Individual<Boolean> individual) {
        double sum = 0;
        for (int i = 0; i < individual.getLearningSamples().size(); i++) {
            Sample<Boolean> learningSample = individual.getLearningSamples().get(i);
            Sample<Boolean> computedSample = individual.getComputedSamples().get(i);
            for (int j = 0; j < learningSample.getOutput().size(); j++) {
                sum += (learningSample.getOutput().get(j) == computedSample.getOutput().get(j)) ? 0 : 10;
            }
        }

        return sum;
    }

    public AdderEvaluator(List<Sample<Boolean>> samples) {
        super(samples);
    }
}
