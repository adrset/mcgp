package cgp.user.app;

import cgp.lib.function.factory.FunctionFactory;
import cgp.lib.individual.Individual;
import cgp.lib.individual.pojo.DataSet;
import cgp.lib.individual.pojo.samples.Sample;
import cgp.lib.simulation.Simulation;
import cgp.lib.simulation.evaluation.AbstractEvaluate;
import cgp.lib.simulation.input.Config;
import cgp.user.function.factory.RandomDoubleSunspotFunctionFactory;
import cgp.user.node.guard.DoubleSunspotGuard;
import cgp.user.simulation.AbsoluteDifferenceEvaluator;

import java.util.List;


public class SunspotsMCGP3 {
    private final Simulation<Double> simulation;

    public SunspotsMCGP3() throws Exception {
        FunctionFactory<Double> factory = new RandomDoubleSunspotFunctionFactory();
        DataSet<Double> dataSet = new DataSet.Builder<Double>().setTargetClass(Double.class).setFileName("series/sunspots_yearly_series_3.json").build();
        Config config = dataSet.getConfig();
        AbstractEvaluate<Double> evaluator = new AbsoluteDifferenceEvaluator(dataSet.getSamples());

        this.simulation = Simulation.Builder.<Double> start()
                .type(Simulation.Type.MCGP)
                .iterations(20)
                .evaluator(evaluator)
                .config(config)
                .randomInitialConnection(false)
                .fileName("mcgp_sunspots_yearly_series.csv")
                .guard(new DoubleSunspotGuard())
                .factory(factory)
                .defaultValue(0.0)
                .build();

    }

    public void run() throws Exception {
        this.simulation.start();

        Individual<Double> i = simulation.getFittest();
        i.zero();
        DataSet<Double> dataSet = new DataSet.Builder<Double>().setTargetClass(Double.class).setFileName("series/sunspots_yearly_series_test.json").build();
        for (int j = 0; j< dataSet.getSamples().size(); j++) {
            Sample<Double> s = dataSet.getSamples().get(j);
            List<Double> otp = i.computeSample(s);
            System.out.printf("%d;%f\n", j,otp.get(0));
        }
    }

    public static void main(String[] args) throws Exception {
        new SunspotsMCGP3().run();
    }
}
