package cgp.user.app;

import cgp.lib.function.factory.FunctionFactory;
import cgp.lib.individual.Individual;
import cgp.lib.individual.pojo.DataSet;
import cgp.lib.individual.pojo.samples.Sample;
import cgp.lib.simulation.Simulation;
import cgp.lib.simulation.evaluation.AbstractEvaluate;
import cgp.lib.simulation.input.Config;
import cgp.user.function.factory.RandomDoubleSunspotFunctionFactory;
import cgp.user.node.guard.DoubleSunspotGuard;
import cgp.user.simulation.AbsoluteDifferenceEvaluator;

import java.util.List;


public class Biths {
    private final Simulation<Double> simulation;

    public Biths() throws Exception {
        FunctionFactory<Double> factory = new RandomDoubleSunspotFunctionFactory();
        DataSet<Double> dataSet = new DataSet.Builder<Double>().setTargetClass(Double.class).setFileName("series/births_series.json").build();
        Config config = dataSet.getConfig();
        AbstractEvaluate<Double> evaluator = new AbsoluteDifferenceEvaluator(dataSet.getSamples());

        this.simulation = Simulation.Builder.<Double> start()
                .type(Simulation.Type.RCGP)
                .iterations(20)
                .evaluator(evaluator)
                .config(config)
                .fileName("birth_series.csv")
                .guard(new DoubleSunspotGuard())
                .factory(factory)
                .defaultValue(0.0)
                .build();

    }

    public void run() throws Exception {
        this.simulation.start();

        Individual<Double> i = simulation.getFittest();
        i.zero();
        DataSet<Double> dataSet = new DataSet.Builder<Double>().setTargetClass(Double.class).setFileName("series/birth_series.json").build();
        for (int j = 0; j< dataSet.getSamples().size(); j++) {
            Sample<Double> s = dataSet.getSamples().get(j);
            List<Double> otp = i.computeSample(s);
            System.out.printf("%d;%f\n", j,otp.get(0));
        }
    }

    public static void main(String[] args) throws Exception {
        new Biths().run();
    }
}
