package cgp.user.app;

import cgp.lib.function.factory.FunctionFactory;
import cgp.lib.individual.pojo.DataSet;
import cgp.lib.simulation.Simulation;
import cgp.lib.simulation.evaluation.AbstractEvaluate;
import cgp.lib.simulation.input.Config;
import cgp.user.function.factory.RandomDoubleFunctionFactory;
import cgp.user.node.guard.DoubleSunspotGuard;
import cgp.user.simulation.FibEvaluator;

public class MainMemory {

    private final Simulation<Double> simulation;

    public MainMemory() throws Exception {
        FunctionFactory<Double> factory = new RandomDoubleFunctionFactory();
        DataSet<Double> dataSet = new DataSet.Builder<Double>().setTargetClass(Double.class).setFileName("fib2.json").build();
        Config config = dataSet.getConfig();
        AbstractEvaluate<Double> evaluator = new FibEvaluator(dataSet.getSamples());
        this.simulation = Simulation.Builder.<Double> start()
                .type(Simulation.Type.RCGP)
                .iterations(1)
                .evaluator(evaluator)
                .config(config)
                .fileName("fib2.csv")
                .guard(new DoubleSunspotGuard())
                .factory(factory)
                .defaultValue(0.0)
                .build();

    }

    public void run() throws Exception {
        this.simulation.start();
        System.out.println(1);
    }

    public static void main(String[] args) throws Exception {
        new MainMemory().run();
    }
}
