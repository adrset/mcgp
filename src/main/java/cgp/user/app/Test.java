package cgp.user.app;

import cgp.lib.function.factory.FunctionFactory;
import cgp.lib.individual.Individual;
import cgp.lib.individual.pojo.DataSet;
import cgp.lib.node.Node;
import cgp.lib.node.NodeWithMemory;
import cgp.lib.node.factory.MemoryNodeFactory;
import cgp.lib.simulation.Simulation;
import cgp.lib.simulation.input.Config;
import cgp.lib.simulation.mutator.connection.InitialRandomConnectionMutator;
import cgp.lib.simulation.mutator.connection.memory.RandomMemoryConnectionMutator;
import cgp.lib.simulation.mutator.function.FunctionMutator;
import cgp.user.function.factory.RandomDoubleSunspotFunctionFactory;
import cgp.user.node.guard.DoubleSunspotGuard;

import java.util.List;


public class Test {
    private Simulation<Double> simulation;

    public Test() throws Exception {
        FunctionFactory<Double> factory = new RandomDoubleSunspotFunctionFactory();
        DataSet<Double> dataSet = new DataSet.Builder<Double>().setTargetClass(Double.class).setFileName("series/sunspots_yearly_series.json").build();
        Config config = dataSet.getConfig();
//
//        this.simulation = Simulation.Builder.<Double> start()
//                .type(Simulation.Type.MCGP)
//                .iterations(1)
//                .evaluator(new FibEvaluator(dataSet.getSamples()))
//                .config(config)
//                .fileName("testing.csv")
//                .guard(new DoubleSunspotGuard())
//                .factory(factory)
//                .defaultValue(0.0)
//                .build();
//

        Individual<Double> ind = new Individual<>(config, new MemoryNodeFactory<Double>(config, factory, 0.0), dataSet.getSamples(), new DoubleSunspotGuard());
        ind.init();
        zero(config, ind);

        InitialRandomConnectionMutator<Double> ircm = new InitialRandomConnectionMutator<>(config);
        ircm.mutate(ind);
       // ind.computeSamples();
        FunctionMutator<Double>functionMutator = new FunctionMutator<>(config, factory);
        RandomMemoryConnectionMutator<Double> rmcm = new RandomMemoryConnectionMutator<>(config);
        for (int i = 0; i < 100; i++) {
            rmcm.mutate(ind);
            functionMutator.mutate(ind);

            ind.computeSample(dataSet.getSamples().get(dataSet.getSamples().size() -1));
            //zero(config, ind);
        }
    }

    private static void zero(Config config, Individual<Double> ind) {
        List<Node<Double>> allNodes = ind.getAllNodes();
        for (int i = 1; i < allNodes.size() - 1; i++) {
            NodeWithMemory<Double> nwm = (NodeWithMemory<Double>) allNodes.get(i);
            for (int j = 0; j < config.getMemoryLength(); j++) {
                nwm.remember((j + 1) * 10.0);
            }
        }
    }

    public void run() throws Exception {
//        this.simulation.run();
//
//        Individual<Double> i = simulation.getFittest();
//
//        for (double j =0; j<3;j++) {
//            Sample<Double> s = new Sample<>();
//            s.setInput(List.of(1.0));
//            List<Double> otp = i.computeSample(s);
//            System.out.printf("%.0f %f\n", j,otp.get(0));
//        }
    }

    public static void main(String[] args) throws Exception {
        new Test().run();
    }
}
