package cgp.user.app;

import cgp.lib.function.factory.FunctionFactory;
import cgp.lib.individual.pojo.DataSet;
import cgp.lib.simulation.Simulation;
import cgp.lib.simulation.input.Config;
import cgp.user.function.factory.RandomBooleanFunctionFactory;
import cgp.user.simulation.AdderEvaluator;

public class MainBoolean {
    private final Simulation<Boolean> simulation;
    public MainBoolean() throws Exception {
        FunctionFactory<Boolean> factory = new RandomBooleanFunctionFactory();
        DataSet<Boolean> dataSet = new DataSet.Builder<Boolean>().setTargetClass(Boolean.class).setFileName("parity4.json").build();
        Config config = dataSet.getConfig();
        this.simulation = Simulation.Builder.<Boolean> start()
                .type(Simulation.Type.CGP)
                .iterations(1)
                .evaluator(new AdderEvaluator(dataSet.getSamples()))
                .config(config)
                .fileName("parity4.csv")
                .factory(factory)
                .defaultValue(false)
                .build();

    }

    public void run() throws Exception{
        this.simulation.start();
    }

    public static void main(String[] args) throws Exception {
        new MainBoolean().run();
    }
}
