FROM openjdk:12-alpine

COPY build/libs/1-all*.jar /cgp.jar
ENV JAR_ARGS="arg1 arg2 arg3"

ENTRYPOINT java -jar cgp.jar ${JAR_ARGS}