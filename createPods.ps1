$n = 5
$yamlFile = "pods/mcgp.yaml"

for ($i = 1; $i -le $n; $i++) {
  $name = "mcgp-$i"
  (Get-Content $yamlFile) -replace 'REPLACE_ME', $name | kubectl apply -f -
}

$yamlFile = "pods/mrcgp.yaml"

for ($i = 1; $i -le $n; $i++) {
  $name = "mcgp-$i"
  (Get-Content $yamlFile) -replace 'REPLACE_ME', $name | kubectl apply -f -
}

$yamlFile = "pods/rcgp.yaml"

for ($i = 1; $i -le $n; $i++) {
  $name = "mcgp-$i"
  (Get-Content $yamlFile) -replace 'REPLACE_ME', $name | kubectl apply -f -
}
